from distutils.core import setup


setup(
    name='bionlp-st-py',
    version='0.1',
    description='Library for parsing and writing BioNLP-ST files.',
    author='Robert Bossy',
    author_email='Robert.Bossy@inrae.fr',
    url='https://forgemia.inra.fr/bibliome/bionlp-st-py',
    packages=['bionlpst']
)
