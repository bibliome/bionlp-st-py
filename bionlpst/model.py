#!/usr/bin/python3

import enum
import os
import os.path
import inspect
import functools
from abc import abstractmethod
from collections.abc import Callable, Iterable, Sequence
from typing import Optional, TextIO


class Location:
    """A location points to the source of a located object."""
    def __init__(self, filename: str, lineno: int):
        self._filename = filename
        self._lineno = lineno

    @staticmethod
    def get(location: Optional['Location'] = None) -> 'Location':
        """
        Returns a location. If location is None, the returns a location that points to the caller source location,
        otherwise returns location.
        """
        if location is None:
            fi = inspect.getframeinfo(inspect.currentframe().f_back.f_back)
            return Location(fi.filename, fi.lineno)
        return location

    @property
    def filename(self) -> str:
        """Location filename."""
        return self._filename

    @property
    def lineno(self) -> int:
        """Location line number."""
        return self._lineno

    def __str__(self):
        return f'{self.filename}:{self.lineno}'


class Located:
    """A located object has a location."""
    def __init__(self, location: Optional[Location] = None):
        self._location = Location.get(location)

    @property
    def location(self) -> Location:
        return self._location


class DuplicateIdentifierError(Exception):
    """Raised when an identifier is specified more than once. Used for documents or annotations."""
    def __init__(self, *args):
        Exception.__init__(self, *args)


class OffsetError(ValueError):
    """Raised when a text-bound offset is invalid (lower than zero or greater than the document length)."""
    def __init__(self, *args):
        ValueError.__init__(self, *args)


class EquivalenceError(Exception):
    """Raised when an annotation is included in an equivalence more than once."""
    def __init__(self, *args):
        Exception.__init__(self, *args)


class Corpus:
    """Corpus object"""
    def __init__(self):
        self._documents: dict[str, Document] = {}

    def get_document(self, docid: str) -> 'Document':
        """
        Returns the Document for the specified document identifier.

        Parameters:
            docid (str) : document identifier

        Returns:
            Document: the document.

        Raises:
            KeyError: no document with the provided identifier in this corpus.
        """
        return self._documents[docid]

    @property
    def documents(self) -> Iterable['Document']:
        """
        Returns an iterator of all documents.
        """
        return self._documents.values()

    def new_document(self, filename: str, docid: str, text: str) -> 'Document':
        """
        Creates a new document in this corpus.

        Parameters:
            filename (str): name of the file where the contents was read.
            docid (str): identifier of the new document.
            text (str): contents of the document.

        Returns:
            Document: the new document.
            does not create a document, and returns the previously known document.

        Raises:
            DuplicateIdentifierError: the corpus already contains a document with the same identifier.
        """
        location = Location(filename, -1)
        if docid in self._documents:
            doc = self._documents[docid]
            raise DuplicateIdentifierError(f'duplicate document {docid} ({str(doc.location)})')
        doc = Document(docid, text, location)
        self._documents[docid] = doc
        return doc

    def _write_asets(self, selector: 'Selector', dirname: Optional[str]):
        if dirname is not None:
            for doc in self.documents:
                aset = doc.annotation_set(selector)
                aset.write(dirname)

    def _write_texts(self, dirname: Optional[str]):
        if dirname is not None:
            for doc in self.documents:
                doc.write_text(dirname)

    def write(self, *, text_dir: Optional[str] = None, input_dir: Optional[str] = None,
              reference_dir: Optional[str] = None, prediction_dir: Optional[str] = None):
        self._write_texts(text_dir)
        self._write_asets(Selector.INPUT, input_dir)
        self._write_asets(Selector.REFERENCE, reference_dir)
        self._write_asets(Selector.PREDICTION, prediction_dir)


class Selector(enum.Enum):
    """
    Annotation Set selector (input, reference, or prediction).
    """
    INPUT = 'input'
    REFERENCE = 'reference'
    PREDICTION = 'prediction'

    @property
    def file_ext(self) -> str:
        """File extension for the selector (.a1 or .a2)."""
        if self is Selector.INPUT:
            return '.a1'
        return '.a2'


class AnnotationSet:
    """Annotation container."""
    
    def __init__(self, doc: 'Document', selector: Selector):
        self._document: Document = doc
        self._selector: Selector = selector
        self._annotations: dict[str, Annotation] = {}
        self._equivalences: list[Equivalence] = []

    @property
    def document(self) -> 'Document':
        """Document to which this annotation set belongs."""
        return self._document

    @property
    def selector(self) -> Selector:
        """Selector of this annotation set."""
        return self._selector

    @property
    def equivalences(self) -> Iterable['Equivalence']:
        """Equivalences in this annotation set."""
        yield from self._equivalences

    def get_annotation(self, id_: str) -> Optional['Annotation']:
        """
        Returns the annotation with the specified identifier.
        If the annotation is not found in this annotation set, then looks in the annotation set with selector INPUT
        in the same document.

        Parameters:
            id_: the identifier to look for.

        Returns:
            Annotation: the annotation.
            None: if the annotation was not found.
        """
        try:
            return self._annotations[id_]
        except KeyError:
            if self.selector == Selector.INPUT:
                return None
            return self.document.annotation_set(Selector.INPUT).get_annotation(id_)

    def annotations(self, *args) -> Iterable['Annotation']:
        """
        Returns annotations in this annotation set.

        Parameter:
            args: optional filters, by default no filter.
                If several filters are specified, then the returned annotations satisfy all filters.
                If the filter is a callable, then it is called for each annotation, this only returns annotations for
                which the return value is true.
                If the filter is a subclass of Annotation, then this only returns annotations of this class.
                If the filter is a string, then this returns only annotations of the specified type.

        Returns:
            iterator of Annotations
        """
        filter_ = Annotation.make_filter(*args)
        for a in self._annotations.values():
            if filter_(a):
                yield a

    def text_bounds(self, *args) -> Iterable['TextBound']:
        """
        Returns text bound annotations in this annotation set.

        Parameter:
            args: optional filters, by default no filter. See AnnotationSet() documentation for filters.

        Returns:
            iterator of TextBound
        """
        filter_ = Annotation.make_filter(*args)
        for a in self._annotations.values():
            if isinstance(a, TextBound) and filter_(a):
                yield a

    def normalizations(self, *args) -> Iterable['Normalization']:
        """
        Returns normalization annotations in this annotation set.

        Parameter:
            args: optional filters, by default no filter. See AnnotationSet() documentation for filters.

        Returns:
            iterator of Normalization
        """
        filter_ = Annotation.make_filter(*args)
        for a in self._annotations.values():
            if isinstance(a, Normalization) and filter_(a):
                yield a

    def relations(self, *args) -> Iterable['Relation']:
        """
        Returns relation annotations in this annotation set.

        Parameter:
            args: optional filters, by default no filter. See AnnotationSet() documentation for filters.

        Returns:
            iterator of Relation
        """
        filter_ = Annotation.make_filter(*args)
        for a in self._annotations.values():
            if isinstance(a, Relation) and filter_(a):
                yield a

    def _check_new_annotation_id(self, id_: str):
        previous = self.get_annotation(id_)
        if previous is not None:
            raise DuplicateIdentifierError(f'duplicate annotation id {id_} ({str(previous.location)})')

    def new_equivalence(self, location: Optional[Location] = None):
        """
        Creates a new equivalence in this annotation set.

        Parameters:
            location: location of the equivalence.

        Returns:
            An Equivalence.
        """
        location = Location.get(location)
        equiv = Equivalence(self, location)
        self._equivalences.append(equiv)
        return equiv

    def new_text_bound(self, type_: str, frags: Iterable[tuple[int, int]], *,
                       location: Optional[Location] = None, id_: Optional[str] = None) -> 'TextBound':
        """
        Creates a new text-bound annotation in this annotation set.

        Parameters:
            type_: type of the annotation.
            frags: a list of spans.
            location: location.
            id_: identifier of the annotation. If not provided, one is creaated automatically.

        Returns:
            A TextBound.

        Raises:
            DuplicateIdentifierError: an annotation with the same identifier exists in this annotation set of in the
            input annotation set of the same document.
            OffsetError: one offset in the spans is invalid.
        """
        location = Location.get(location)
        id_ = self._document.next_text_bound_id.get(id_)
        self._check_new_annotation_id(id_)
        result = TextBound(self, id_, type_, frags, location)
        self._annotations[id_] = result
        return result

    def new_normalization(self, type_: str, referent: str,
                          *, annotation: Optional['Annotation'] = None,
                          location: Optional[Location] = None, id_: Optional[str] = None
                          ) -> 'Normalization':
        """
        Creates a new normalization annotation in this annotation set.

        Parameters:
            type_: type of the annotation.
            referent: identifier of an element in the external resource.
            annotation: normalized annotation.
            location: location.
            id_: identifier of the annotation. If not provided, one is creaated automatically.

        Returns:
            A Normalization.

        Raises:
            DuplicateIdentifierError: an annotation with the same identifier exists in this annotation set of in the
            input annotation set of the same document.
        """
        location = Location.get(location)
        id_ = self._document.next_normalization_id.get(id_)
        self._check_new_annotation_id(id_)
        result = Normalization(self, id_, type_, referent, annotation=annotation, location=location)
        self._annotations[id_] = result
        return result

    def new_relation(self, type_: str,
                     *,
                     arguments: Optional[Sequence[tuple[str, 'Annotation']]] = None,
                     location: Optional[Location] = None, id_: Optional[str] = None
                     ) -> 'Relation':
        """
        Creates a new relation annotation in this annotation set.

        Parameters:
            type_: type of the annotation.
            arguments: a tuple of key/argument pairs.
            location: location.
            id_: identifier of the annotation. If not provided, one is creaated automatically.

        Returns:
            A Relation.

        Raises:
            DuplicateIdentifierError: an annotation with the same identifier exists in this annotation set of in the
            input annotation set of the same document.
        """
        location = Location.get(location)
        id_ = self._document.next_relation_id.get(id_)
        self._check_new_annotation_id(id_)
        result = Relation(self, id_, type_, arguments=arguments, location=location)
        self._annotations[id_] = result
        return result

    def new_dummy(self, id_: str, type_: str, location: Optional[Location] = None) -> 'Dummy':
        location = Location.get(location)
        self._check_new_annotation_id(id_)
        result = Dummy(self, id_, type_, location)
        self._annotations[id_] = result
        return result

    def new_malformed(self, id_: str, type_: str, location: Optional[Location] = None) -> 'Malformed':
        location = Location.get(location)
        self._check_new_annotation_id(id_)
        result = Malformed(self, id_, type_, location)
        self._annotations[id_] = result
        return result

    def write(self, dirname: str):
        os.makedirs(dirname, exist_ok=True)
        filename = os.path.join(dirname, self._document.docid + self._selector.file_ext)
        with open(filename, 'w') as f:
            for a in self.annotations():
                a.write(f)
            for e in self._equivalences:
                e.write(f)


class NextId:
    """Annotation identifier provider."""
    def __init__(self, prefix: str):
        self._prefix = prefix
        self._next: int = 1

    def get(self, id_: Optional[str] = None) -> str:
        """
        Get an id.

        Parameters:
            id_: if not None returns this value.

        Returns:
            The provided identifier if not None. Otherwise the next available identifier.
        """
        if id_ is None:
            result = self._prefix + str(self._next)
            self._next += 1
            return result
        return id_

    def set(self, n: int):
        """Set the next available identifier."""
        self._next = max(self._next, n + 1)


class Document(Located):
    """Document object."""
    def __init__(self, docid: str, text: str, location: Optional[Location] = None):
        Located.__init__(self, location)
        self._docid: str = docid
        self._annotation_sets: dict[Selector, AnnotationSet] = dict((sel, AnnotationSet(self, sel)) for sel in Selector)
        self._text: str = text
        self._next_text_bound_id: NextId = NextId('T')
        self._next_normalization_id: NextId = NextId('N')
        self._next_relation_id: NextId = NextId('R')

    @property
    def docid(self) -> str:
        """Document identifier."""
        return self._docid

    @property
    def text(self) -> str:
        """Text content of the document."""
        return self._text

    @property
    def next_text_bound_id(self) -> NextId:
        """Identifier provider for text-bound annotations."""
        return self._next_text_bound_id

    @property
    def next_normalization_id(self) -> NextId:
        """Identifier provider for normalization annotations."""
        return self._next_normalization_id

    @property
    def next_relation_id(self) -> NextId:
        """Identifier provider for relation annotations."""
        return self._next_relation_id

    def annotation_set(self, selector: Selector) -> AnnotationSet:
        """Returns the annotation set for the specified selector."""
        return self._annotation_sets[selector]

    @property
    def input(self) -> AnnotationSet:
        """Input annotation set."""
        return self.annotation_set(Selector.INPUT)

    @property
    def reference(self) -> AnnotationSet:
        """Reference annotation set."""
        return self.annotation_set(Selector.REFERENCE)

    @property
    def prediction(self) -> AnnotationSet:
        """Prediction annotation set."""
        return self.annotation_set(Selector.PREDICTION)

    def accept_offset(self, off: int):
        """
        Checks if the offset is valid for this document.

        Parameters:
            off: offset to check.

        Raises:
             OffsetError: the offset is invalid.
        """
        if off < 0:
            raise OffsetError('offset < 0')
        if off > len(self.text):
            raise OffsetError(f'offset > length ({off} > {len(self.text)})')

    def write_text(self, dirname: str):
        os.makedirs(dirname, exist_ok=True)
        filename = os.path.join(dirname, f'{self.docid}.txt')
        with open(filename, 'w') as f:
            f.write(self._text)


class Equivalence(Located):
    """Annotation equivalence set."""
    def __init__(self, aset: AnnotationSet, location: Optional[Location] = None):
        Located.__init__(self, location)
        self._annotations: Optional[set[Annotation]] = set()
        self._aset = aset

    @property
    def annotations(self) -> Iterable['Annotation']:
        """Annotations in this equivalence."""
        yield from self._annotations

    def add_annotation(self, a: 'Annotation'):
        """
        Adds an annotation to this equivalence.

        Parameters:
            a: annotation to add.

        Raises:
            EquivalenceError: th annotation is already in an equivalence.
        """
        if a.equivalence is not None:
            if a.equivalence is self:
                raise EquivalenceError(f'duplicate annotation in equivalence ({a.id_}, {str(a.equivalence.location)})')
            raise EquivalenceError(f'equivalence overlaps ({a.id_}, {str(a.equivalence.location)})')
        self._annotations.add(a)
        a.equivalence = self

    @property
    def aset(self) -> AnnotationSet:
        """Annotation set to which this equivalence belongs."""
        return self._aset

    def write(self, f: TextIO):
        f.write(f'*\t{" ".join(a.id_ for a in self.annotations)}\n')


class Annotation(Located):
    """Annotation base class."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, location: Optional[Location]):
        Located.__init__(self, location)
        self._aset: AnnotationSet = aset
        self._id: str = id_
        self._type: str = type_
        self._back_references: list[Annotation] = []
        self._equivalence: Optional[Equivalence] = None

    @property
    def aset(self) -> AnnotationSet:
        """Annotation set to which this annotation belongs."""
        return self._aset

    @property
    def id_(self) -> str:
        """Annotation identifier."""
        return self._id

    @property
    def type_(self) -> str:
        """Annotation type."""
        return self._type

    def back_references(self, *args) -> Iterable['Annotation']:
        """Annotations (normalization or relation) that reference this annotation."""
        filter_ = Annotation.make_filter(*args)
        for a in self._back_references:
            if filter_(a):
                yield a

    def _add_back_reference(self, annotation: 'Annotation'):
        self._back_references.append(annotation)

    @property
    def equivalence(self) -> Optional[Equivalence]:
        """Equivalence containing this annotation. Can be None."""
        return self._equivalence

    @equivalence.setter
    def equivalence(self, equivalence: Equivalence):
        self._equivalence = equivalence

    @abstractmethod
    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        pass

    def _write(self, f: TextIO, rest: str):
        f.write(f'{self._id}\t{self._type} {rest}\n')

    @abstractmethod
    def write(self, f: TextIO):
        pass

    @staticmethod
    def make_filter(*args) -> Callable[['Annotation'], bool]:
        args = list(arg for arg in args if arg is not None)
        if len(args) == 0:
            return lambda _: True
        if len(args) == 1:
            arg, = args
            if isinstance(arg, Callable):
                return arg
            if isinstance(arg, type):
                return lambda a: isinstance(a, arg)
            if isinstance(arg, Selector):
                return lambda a: a.aset.selector == arg
            if isinstance(arg, str):
                return lambda a: a.type_ == arg
            if isinstance(arg, Sequence):
                filters = list(Annotation.make_filter(a) for a in arg)
                return lambda a: any(f(a) for f in filters)
            raise ValueError(f'cannot convert to predicate: {arg}')
        filters = list(Annotation.make_filter(arg) for arg in args)
        return lambda a: all(f(a) for f in filters)


class Dummy(Annotation):
    """Dummy annotation created only as placeholders for unresolved annotation references."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, location: Optional[Location] = None):
        Annotation.__init__(self, aset, id_, type_, location)

    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        return ()

    def write(self, f: TextIO):
        pass


class Malformed(Annotation):
    """Malformed annotation created only as placeholders for annotations that could not be parsed."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, location: Optional[Location] = None):
        Annotation.__init__(self, aset, id_, type_, location)

    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        return ()

    def write(self, f: TextIO):
        pass


class TextBound(Annotation):
    FORM_ESCAPE_TRANS = str.maketrans({'\n': '\\n', '\r': '\\r', '\t': '\\t', '\\': '\\\\'})

    """Text-bound annotation."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, frags: Iterable[tuple[int, int]],
                 location: Optional[Location] = None):
        Annotation.__init__(self, aset, id_, type_, location)
        self._fragments: tuple[tuple[int, int], ...] = tuple(TextBound._check_fragments(aset.document, frags))
        self._form: str = ' '.join(aset.document.text[start:end] for (start, end) in self.fragments)

    @staticmethod
    def _check_fragments(doc: Document, frags: Iterable[tuple[int, int]]):
        last_end = -1
        for start, end in frags:
            doc.accept_offset(start)
            doc.accept_offset(end)
            if end == start:
                raise OffsetError(f'empty fragment ({start})')
            if end < start:
                raise OffsetError(f'invalid fragment {start} > {end}')
            if start <= last_end:
                raise OffsetError(f'overlapping fragments {start} <= {last_end}')
            last_end = end
            yield start, end

    @property
    def fragments(self) -> tuple[tuple[int, int], ...]:
        """Text fragments."""
        return self._fragments

    @property
    def form(self) -> str:
        """Text-bound surface form."""
        return self._form

    def __len__(self):
        return sum((end - start) for (start, end) in self._fragments)

    def __str__(self):
        sfrags = ';'.join(f'{start} {end}' for (start, end) in self._fragments)
        return f'{self.type_} \'{self._form}\' {sfrags}'

    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        return self._fragments

    def write(self, f: TextIO):
        sfrags = ';'.join(f'{start} {end}' for (start, end) in self._fragments)
        form = self._form.translate(TextBound.FORM_ESCAPE_TRANS)
        self._write(f, f'{sfrags}\t{form}')


class Normalization(Annotation):
    """Normalization annotation."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, referent: str, *,
                 annotation: Optional[Annotation] = None, location: Optional[Location] = None):
        Annotation.__init__(self, aset, id_, type_, location)
        self._annotation: Optional[Annotation] = annotation
        self._referent: str = referent

    @property
    def annotation(self) -> Annotation:
        """Normalized annotation."""
        assert self._annotation is not None
        return self._annotation

    @annotation.setter
    def annotation(self, annotation: Annotation):
        if self._annotation is not None:
            raise ValueError(f'normalization annotation ({self.id_}) is already set ({str(self.location)})')
        self._annotation = annotation
        annotation._add_back_reference(self)

    @property
    def referent(self) -> str:
        """Identifier to an element in the external resource."""
        return self._referent

    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        return self.annotation.anchor_fragments()

    def write(self, f: TextIO):
        self._write(f, f'Annotation:{self.annotation.id_} Referent:{self.referent}')


class Relation(Annotation):
    """Relation annotation."""
    def __init__(self, aset: AnnotationSet, id_: str, type_: str, *,
                 arguments: Optional[Sequence[tuple[str, Annotation]]] = None, location: Optional[Location] = None):
        Annotation.__init__(self, aset, id_, type_, location)
        self._arguments: Optional[dict[str, Annotation]] = dict(() if arguments is None else arguments)

    @property
    def arguments(self) -> dict[str, Annotation]:
        """Relation arguments."""
        return dict(self._arguments)

    def set_argument(self, role: str, arg: Annotation):
        """
        Set an argument.

        Parameters:
            role: argument role.
            arg: argument.

        Raises:
            ValueError: this relation has already an argument for the specified role.
        """
        if role in self._arguments:
            raise ValueError(f'relation argument ({role}) is already set ({str(self.location)})')
        self._arguments[role] = arg
        arg._add_back_reference(self)

    def anchor_fragments(self) -> Sequence[tuple[int, int]]:
        result = []
        for arg in self.arguments.values():
            result.extend(arg.anchor_fragments())
        result.sort(key=lambda f: (f[0], -f[1]))
        return result

    def write(self, f: TextIO):
        sargs = ' '.join(f'{k}:{arg.id_}' for k, arg in self.arguments.items())
        self._write(f, sargs)


__all__ = [
    'Location',
    'Located',
    'DuplicateIdentifierError',
    'OffsetError',
    'EquivalenceError',
    'Corpus',
    'Selector',
    'AnnotationSet',
    'Document',
    'Equivalence',
    'Annotation',
    'Dummy',
    'Malformed',
    'TextBound',
    'Normalization',
    'Relation',
]
