from typing import Optional
from collections.abc import Iterable, Sequence

from .model import *

from evaluate.similarity import *
from evaluate.pairing import MunkresPairing, Pair
from evaluate.scoring import BaseScores, IEScores, Scores


def text_bound_similarity(offset_similarity: Similarity = multi_span_jaccard,
                          type_similarity: Similarity = equal,
                          normalization_similarities: Optional[dict[str, Similarity]] = None) -> Similarity:
    def similarity(ref: TextBound, pred: TextBound) -> float:
        result = offset_similarity(ref.fragments, pred.fragments)
        result *= type_similarity(ref.type_, pred.type_)
        if normalization_similarities is not None:
            for norm_type, norm_sim in normalization_similarities.items():
                ref_norms = set(n.referent for n in ref.back_references(Normalization, Selector.REFERENCE, norm_type))
                pred_norms = set(n.referent for n in pred.back_references(Normalization, Selector.PREDICTION, norm_type))
                result *= norm_sim(ref_norms, pred_norms)
        return result
    return similarity


class TextBoundEvaluator:
    def __init__(self,
                 offset_similarity: Similarity = multi_span_jaccard,
                 type_similarity: Similarity = equal,
                 normalization_similarities: Optional[dict[str, Similarity]] = None,
                 types: Optional[Sequence[str]] = None
                 ):
        self._similarity = text_bound_similarity(offset_similarity, type_similarity, normalization_similarities)
        self._pairing = MunkresPairing[TextBound](self._similarity)
        self._types = None if types is None else list(types)

    def get_doc_pairs(self, doc: Document) -> Iterable[Pair[TextBound]]:
        refs = list(doc.reference.text_bounds(self._types))
        preds = list(doc.prediction.text_bounds(self._types))
        yield from self._pairing.get_pairs(refs, preds)

    def get_corpus_pairs(self, corpus: Corpus) -> Iterable[Pair[TextBound]]:
        for doc in corpus.documents:
            yield from self.get_doc_pairs(doc)

    def evaluate(self, pairs: Sequence[Pair[TextBound]]) -> dict[str, Scores]:
        base = BaseScores(pairs)
        strict = IEScores(pairs, base=base)
        relaxed = IEScores(pairs, tp_attr='couples', base=base)
        return dict(base=base, strict=strict, relaxed=relaxed)

    def evaluate_doc(self, doc: Document) -> tuple[Sequence[Pair[TextBound]], dict[str, Scores]]:
        pairs = list(self.get_doc_pairs(doc))
        scores = self.evaluate(pairs)
        return pairs, scores

    def evaluate_corpus(self, corpus: Corpus) -> tuple[Sequence[Pair[TextBound]], dict[str, Scores]]:
        pairs = list(self.get_corpus_pairs(corpus))
        scores = self.evaluate(pairs)
        return pairs, scores


def strict_relation_similarity(ref: Relation, pred: Relation) -> float:
    if ref.type_ != pred.type_:
        return 0.0
    for k, v in ref.arguments.items():
        if k not in pred.arguments:
            return 0.0
        if pred.arguments[k] is not v:
            return 0.0
    return 1.0


def ignore_roles_relation_similarity(check_type: bool) -> Similarity:
    def similarity(ref: Relation, pred: Relation) -> float:
        if check_type and ref.type_ != pred.type_:
            return 0.0
        if set(ref.arguments.values()) == set(pred.arguments.values()):
            return 1.0
        return 0.0
    return similarity


class RelationEvaluator:
    def __init__(self, types: Optional[Sequence[str]] = None, ignore_roles: bool = False, ignore_type: bool = False):
        if ignore_roles or ignore_type:
            self._similarity = ignore_roles_relation_similarity(not ignore_type)
        else:
            self._similarity = strict_relation_similarity
        self._pairing = MunkresPairing[Relation](self._similarity)
        self._types = types

    def get_doc_pairs(self, doc: Document) -> Iterable[Pair[Relation]]:
        refs = list(doc.reference.relations(self._types))
        preds = list(doc.prediction.relations(self._types))
        yield from self._pairing.get_pairs(refs, preds)

    def get_corpus_pairs(self, corpus: Corpus) -> Iterable[Pair[Relation]]:
        for doc in corpus.documents:
            yield from self.get_doc_pairs(doc)

    def evaluate(self, pairs: Sequence[Pair[Relation]]) -> dict[str, Scores]:
        base = BaseScores(pairs)
        strict = IEScores(pairs, base=base)
        relaxed = IEScores(pairs, tp_attr='couples', base=base)
        return dict(base=base, strict=strict, relaxed=relaxed)

    def evaluate_doc(self, doc: Document) -> tuple[Sequence[Pair[Relation]], dict[str, Scores]]:
        pairs = list(self.get_doc_pairs(doc))
        scores = self.evaluate(pairs)
        return pairs, scores

    def evaluate_corpus(self, corpus: Corpus) -> tuple[Sequence[Pair[Relation]], dict[str, Scores]]:
        pairs = list(self.get_corpus_pairs(corpus))
        scores = self.evaluate(pairs)
        return pairs, scores


def normalization_similarity(referent_similarity: Similarity):
    def similarity(ref: Normalization, pred: Normalization) -> float:
        if ref.type_ != pred.type_:
            return 0.0
        if ref.annotation is not pred.annotation:
            return 0.0
        return referent_similarity(ref.referent, pred.referent)
    return similarity


class NormalizationEvaluator:
    def __init__(self, types: Optional[Sequence[str]] = None, referent_similarity: Similarity = equal):
        self._types = types
        self._similarity = normalization_similarity(referent_similarity)

    def get_doc_pairs(self, doc: Document) -> Iterable[Pair[Normalization]]:
        refs = list(doc.reference.normalizations(self._types))
        preds = list(doc.prediction.normalizations(self._types))
        yield from self._pairing.get_pairs(refs, preds)

    def get_corpus_pairs(self, corpus: Corpus) -> Iterable[Pair[Normalization]]:
        for doc in corpus.documents:
            yield from self.get_doc_pairs(doc)

    def evaluate(self, pairs: Sequence[Pair[Normalization]]) -> dict[str, Scores]:
        base = BaseScores(pairs)
        strict = IEScores(pairs, base=base)
        relaxed = IEScores(pairs, tp_attr='couples', base=base)
        return dict(base=base, strict=strict, relaxed=relaxed)

    def evaluate_doc(self, doc: Document) -> tuple[Sequence[Pair[Normalization]], dict[str, Scores]]:
        pairs = list(self.get_doc_pairs(doc))
        scores = self.evaluate(pairs)
        return pairs, scores

    def evaluate_corpus(self, corpus: Corpus) -> tuple[Sequence[Pair[Normalization]], dict[str, Scores]]:
        pairs = list(self.get_corpus_pairs(corpus))
        scores = self.evaluate(pairs)
        return pairs, scores
