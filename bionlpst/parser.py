from abc import abstractmethod
from collections.abc import Iterable
from typing import TextIO, Optional, Literal
from .model import *
import re
import glob
import os
import os.path
import logging


MALFORMED_TYPE_PARSER = re.compile(r'(?P<type>\S+)\s*.*')
TEXT_BOUND_PARSER = re.compile(r'(?P<type>\S+) (?P<fragments>\d+ \d+(?:;\d+ \d+)*)\t(?P<control>.*)')
RELATION_PARSER = re.compile(r'(?P<type>\S+) (?P<args>.+)')
ARGUMENT_PARSER = re.compile(r'\s*(?P<role>[^:]+):(?P<ref>\S+)\s*')
NORMALIZATION_PARSER = re.compile(r'(?P<type>\S+) Annotation:(?P<ann>\S+) Referent:(?P<ref>\S+)')


class AnnotationReference(Located):
    def __init__(self, location: Location, aset: AnnotationSet, ref: str):
        Located.__init__(self, location)
        self._aset = aset
        self._ref = ref

    def _get_annotation(self) -> Annotation:
        result = self._aset.get_annotation(self._ref)
        if result is None:
            logging.error(f'({str(self.location)}) unknown annotation identifier {self._ref}')
            return self._aset.new_dummy(self._ref, 'Dummy', self.location)
        return result

    def resolve(self):
        a = self._get_annotation()
        self.link_annotation(a)

    @abstractmethod
    def link_annotation(self, a: Annotation):
        pass


class NormalizationAnnotationReference(AnnotationReference):
    def __init__(self, location: Location, normalization: Normalization, ref: str):
        AnnotationReference.__init__(self, location, normalization.aset, ref)
        self._normalization = normalization

    def link_annotation(self, a: Annotation):
        self._normalization.annotation = a


class RelationArgumentReference(AnnotationReference):
    def __init__(self, location: Location, relation: Relation, ref: str, role: str):
        AnnotationReference.__init__(self, location, relation.aset, ref)
        self._relation = relation
        self._role = role

    def link_annotation(self, a: Annotation):
        try:
            self._relation.set_argument(self._role, a)
        except ValueError as e:
            logging.error(f'({self.location}) {str(e)}')


class EquivalenceAnnotationReference(AnnotationReference):
    def __init__(self, location: Location, equivalence: Equivalence, ref: str):
        AnnotationReference.__init__(self, location, equivalence.aset, ref)
        self._equivalence = equivalence

    def link_annotation(self, a: Annotation):
        try:
            self._equivalence.add_annotation(a)
        except EquivalenceError as e:
            logging.error(f'({str(self.location)}) {str(e)}')


class BioNLPSTParser:
    WHITESPACE = re.compile('\\s+')

    """BioNLP-ST files parser."""
    def __init__(self, corpus: Optional[Corpus] = None,
                 form_check: Literal['strict', 'ignore_whitespace', 'escaped', 'no_check'] = 'strict'):
        if corpus is None:
            self._corpus = Corpus()
        else:
            self._corpus = corpus
        self._annotation_references: list[AnnotationReference] = []
        self._form_check = form_check

    @property
    def corpus(self) -> Corpus:
        """Corpus."""
        self.resolve_annotation_references()
        return self._corpus

    @corpus.setter
    def corpus(self, corpus: Corpus):
        self._corpus = corpus
        self._annotation_references.clear()

    def resolve_annotation_references(self) -> Corpus:
        """Resolve annotation references."""
        for ar in self._annotation_references:
            ar.resolve()
        self._annotation_references.clear()
        return self._corpus

    @staticmethod
    def _get_docid(filename: str) -> str:
        base = os.path.basename(filename)
        result, ext = os.path.splitext(base)
        return result

    def _parse_text_file(self, filename: str, f: TextIO):
        docid = BioNLPSTParser._get_docid(filename)
        text = f.read()
        try:
            self._corpus.new_document(filename, docid, text)
        except DuplicateIdentifierError as dup:
            location = Location(filename, -1)
            logging.error(f'({str(location)}) {str(dup)}')

    def _parse_text_filename(self, filename: str):
        if os.path.isdir(filename):
            for fn in glob.glob(f'{filename}/*.txt'):
                self._parse_text_filename(fn)
        else:
            with open(filename) as f:
                self._parse_text_file(filename, f)

    def _parse_annotations_file(self, aset: AnnotationSet, filename: str, f: TextIO):
        for lineno, line in enumerate(f):
            if line.strip():
                self._parse_annotation_line(aset, Location(filename, lineno + 1), line[:-1])

    def _parse_annotations_filename(self, selector: Selector, filename: str):
        if os.path.isdir(filename):
            for fn in glob.glob(f'{filename}/*{selector.file_ext}'):
                self._parse_annotations_filename(selector, fn)
        else:
            docid = BioNLPSTParser._get_docid(filename)
            try:
                doc = self._corpus.get_document(docid)
                aset = doc.annotation_set(selector)
                with open(filename) as f:
                    self._parse_annotations_file(aset, filename, f)
            except KeyError:
                logging.error(f'({str(Location(filename, -1))}) unknown document {docid}')

    def _parse_annotation_line(self, aset: AnnotationSet, location: Location, line: str):
        id_, rest = line.split('\t', 1)
        if id_ == '*':
            if aset.selector == Selector.PREDICTION:
                logging.warning(f'({str(location)}) predictions are not supposed to provide equivalences')
            else:
                self._parse_equivalence(aset, location, rest)
        else:
            try:
                id_type = id_[0]
                id_num = int(id_[1:])
                if id_type == 'T' or id_type == 'W':
                    self._parse_text_bound(aset, location, id_, rest)
                    aset.document.next_text_bound_id.set(id_num)
                elif id_type == 'R' or id_type == 'E':
                    self._parse_relation(aset, location, id_, rest)
                    aset.document.next_relation_id.set(id_num)
                elif id_type == 'N':
                    self._parse_normalization(aset, location, id_, rest)
                    aset.document.next_normalization_id.set(id_num)
                else:
                    logging.error(f'({str(location)}) unknown annotation kind')
                    aset.new_malformed(id_, BioNLPSTParser._parse_malformed_type(rest), location)
            except DuplicateIdentifierError as dup:
                logging.error(f'({str(location)}) {str(dup)}')

    def _parse_equivalence(self, aset: AnnotationSet, location: Location, rest: str):
        if not rest.startswith('Equiv '):
            logging.error(f'({str(location)}) ill formed equivalence, form \'Equiv\'')
        else:
            result = aset.new_equivalence(location)
            arefs = rest[6:].split(' ')
            for ar in arefs:
                self._annotation_references.append(EquivalenceAnnotationReference(location, result, ar))

    @staticmethod
    def _parse_malformed_type(rest: str) -> str:
        m = MALFORMED_TYPE_PARSER.fullmatch(rest)
        if m is None:
            return '!Malformed'
        return m.group('type')

    def _parse_text_bound(self, aset: AnnotationSet, location: Location, id_: str, rest: str):
        m = TEXT_BOUND_PARSER.fullmatch(rest)
        if m is None:
            logging.error(f'({str(location)}) ill formed text-bound annotation')
            aset.new_malformed(id_, BioNLPSTParser._parse_malformed_type(rest), location)
        else:
            type_ = m.group('type')
            frags = m.group('fragments')
            ctrl = m.group('control')
            try:
                tb = aset.new_text_bound(type_, BioNLPSTParser._parse_fragments(frags), location=location, id_=id_)
                expected, got = self._check_form_control(tb.form, ctrl)
                if expected != got:
                    logging.warning(f'({str(location)}) form control mismatch (form: "{expected}", got: "{got}")')
            except OffsetError as e:
                logging.error(f'({str(location)}) {str(e)}')

    def _check_form_control(self, form: str, ctrl: str) -> tuple[str, str]:
        if self._form_check == 'strict':
            return form, ctrl
        if self._form_check == 'ignore_whitespace':
            return BioNLPSTParser.WHITESPACE.sub('', form), BioNLPSTParser.WHITESPACE.sub('', ctrl)
        if self._form_check == 'escaped':
            return form.translate(TextBound.FORM_ESCAPE_TRANS), ctrl
        if self._form_check == 'no_check':
            return '', ''
        raise RuntimeError()

    @staticmethod
    def _parse_fragments(frags: str) -> Iterable[tuple[int, int]]:
        for f in frags.split(';'):
            yield tuple(int(off) for off in f.split(' '))

    def _parse_relation(self, aset: AnnotationSet, location: Location, id_: str, rest: str):
        m = RELATION_PARSER.fullmatch(rest)
        if m is None:
            logging.error(f'({str(location)}) ill formed relation annotation')
            aset.new_malformed(id_, BioNLPSTParser._parse_malformed_type(rest), location)
        else:
            type_ = m.group('type')
            args = m.group('args')
            arg_refs = BioNLPSTParser._parse_args(location, args)
            rel = aset.new_relation(type_, location=location, id_=id_)
            for role, ar in arg_refs:
                self._annotation_references.append(RelationArgumentReference(location, rel, ar, role))

    @staticmethod
    def _parse_args(location: Location, args: str) -> list[tuple[str, str]]:
        result = ARGUMENT_PARSER.findall(args)
        if not result:
            logging.warning(f'({str(location)}) empty arguments')
        return result

    def _parse_normalization(self, aset: AnnotationSet, location: Location, id_: str, rest: str):
        m = NORMALIZATION_PARSER.fullmatch(rest)
        if m is None:
            logging.error(f'({str(location)}) ill formed normalization annotation')
            aset.new_malformed(id_, BioNLPSTParser._parse_malformed_type(rest), location)
        else:
            type_ = m.group('type')
            ann = m.group('ann')
            ref = m.group('ref')
            norm = aset.new_normalization(type_, ref, location=location, id_=id_)
            self._annotation_references.append(NormalizationAnnotationReference(location, norm, ann))

    def parse_text(self, dirname: str):
        self._parse_text_filename(dirname)

    def parse_input(self, dirname: str):
        self._parse_annotations_filename(Selector.INPUT, dirname)

    def parse_reference(self, dirname: str):
        self._parse_annotations_filename(Selector.REFERENCE, dirname)

    def parse_corpus_and_reference(self, dirname: str):
        """
        Parse a corpus, input and reference annotations in a directory.
        Reads `*.txt' files, then `*.a1' files, then `*.a2' files in the specified directory.

        Parameters:
            dirname (str): directory path.
        """
        self._parse_text_filename(dirname)
        self._parse_annotations_filename(Selector.INPUT, dirname)
        self._parse_annotations_filename(Selector.REFERENCE, dirname)

    def parse_predictions(self, dirname: str):
        """
        Parse predictions into a corpus.
        """
        self._parse_annotations_filename(Selector.PREDICTION, dirname)


__all__ = [
    'BioNLPSTParser'
]
