# bionlp-st-py

BioNLP-ST format data model and parser in Python.

## Requirements

- Python 3.10

## Data model

### Classes

- `Corpus`: a corpus object represents the set of documents ith their annotations.
- `Document`: a document has a unique (in its corpus) identifier (`docid`) and a text representing its content.
- `AnnotationSet`: an annotation set is a container for annotations, each document has exactly three annotation sets representing the input annotations, the reference annotations and the predicted annotaions respectively.
- `Annotation`: each annotation belongs to a single annotation set, annotations comes in 4 kinds:
  - `TextBound`: entity annotations directly bound to the text, text-bound annotations may be discontinuous and thus be composed of several fragments.
  - `Normalization`: a normalization is composed of an annotation, usually text-bound, and a referent which is an identifier of an item in an external ressource.
  - `Relation`: relations link several annotations, a relation is composed of an arbitrary number of key-annotation pairs, relation arguments may be of any kind.
  - `Dummy`: dummy annotations are created as placeholders for unresolved annotation references.
  - `Malformed`: malformed annotations are created by the parser when it cannot parse a line in BioNLP-ST format.
- `Equivalence`: a set of annotations that are considered equivelent, equivelences in the same document may not contain the same annotations.

### Class diagram

```mermaid
classDiagram
    class Corpus{
    }
    class Document{
        docid: str 
        text: str 
    }
    class AnnotationSet{
    }
    class Annotation{
        id_: str 
        type_: str 
    }
    class TextBound{
        form: str 
    }
    class Normalization{
        referent: str 
    }
    class Relation{
    }
    class Equivalence{
    }
    Corpus "1" *-- "0..n" Document
    Document "1" *-- "3" AnnotationSet : input, reference, prediction
    AnnotationSet "1" *-- "0..n" Annotation
    TextBound --|> Annotation
    Normalization --|> Annotation
    Relation --|> Annotation
    Dummy --|> Annotation
    Malformed --|> Annotation
    Equivalence "0..1" o-- "1..n" Annotation
    Normalization "0..n" o-- "1" Annotation : annotation
    Relation "0..n" o-- "1..n" Annotation : arguments
```

## API

### Parsing BioNLP-ST files

```python=
import bionlpst.parser

parser = bionlpst.parser.BioNLPSTParser()
parser.parse_corpus_and_reference('/path/to/reference')
parser.parse_predictions('/path/to/reference')
corpus = parser.resolve_annotation_references()
```

or

```python=
import bionlpst.parser

parser = bionlpst.parser.BioNLPSTParser()
parser.parse_text('/path/to/text')  # .txt files
parser.parse_input('/path/to/input')  # .a1 files
parser.parse_reference('/path/to/reference')  # .a2 files
parser.parse_predictions('/path/to/predictions')  # .a2 files
corpus = parser.resolve_annotation_references()
```


Remarks:
- the parser creates a document for each .txt file.
- if a .a1 or .a2 file is missing, then the corresponding annotation set is left empty.
- if you only wish to parse input and reference but no predictions, you may skip the call t `parse_predictions()`.
- the call to `resolve_references()` is necessary after parsing, this method links annotations where the parser found annotation identifiers.

#### Warnings and Errors

- (`E`) `argument is already set`: a relation specifies the same argument several times.
- (`E`) `duplicate annotation id`: an annotation identifier is already in use in the document.
- (`E`) `ill formed equivalence, expected 'Equiv'`: equivalence definitions should start with `Equiv`.
- (`E`) `ill formed normalization annotation`: could not parse a normalization.
- (`E`) `ill formed relation annotation`: could not parse a relation.
- (`E`) `ill formed text-bound annotation`: could not parse a text-bound annotation.
- (`E`) `offset > length`: text-bound offset is greater that the document length.
- (`E`) `unknown annotation identifier`: the annotation identifier references an undefined annotation.
- (`E`) `unknown annotation kind`: the annotation identifier does not start with `T` (text-bound), `N` (normalization), or `R` (relation).
- (`E`) `unknown document`: there is no document in the corpus for the specified .a1 or .a2 file.
- (`W`) `duplicate annotation in equivalence`: an equivalence contains several references to the same annotation.
- (`W`) `empty arguments`: a relation has no argument.
- (`W`) `equivalence overlaps`: several equivalences reference the same annotation.
- (`W`) `form control mismatch`: the text-bound form control does not match the text in the fragment offsets. See comment below.
- (`W`) `predictions are not supposed to provide equivalences`: usually equivalences are found either in input or reference annotation set.

#### Form control

Form control is the process that checks that text-bound annotations has the expected form. By default, the check is
strict: the form control must be equal to the annotation form. However, BioNLP-ST files may differ in their treatment of
whitespace.

The `BioNLPSTParser` class accepts the `form_check` parameter that allows to specify the form control checking. This
parameter may take the following values:

- `strict`: strict check, the form control and the annotation form must be strictly equal (default)
- `ignore_whitespace`: the form control check does not issue an error for whitespace differences, this allows to
validate files where whitespace characters are normalized.
- `escaped`: the form control is supposed to be escaped, sequences `\n`, `\r`, `\t` and `\\` are interpreted as newline,
carriage return, horizontal tab, and `\` respectively.
- `no_check`: do not parform form control at all.

### Navigating the data

#### Object atttributes and methods

`Corpus`
- `documents`: Document iterator.
- `get_document(id_)`: Retrieve document by identifier (raises `KeyError` if not found).

`Document`
- `docid`: document identifier.
- `text`: document text content.
- `input`: input annotation set.
- `reference`: reference annotation set.
- `prediction`: prediction annotation set.

`AnnotationSet`
- `document`: uplink document.
- `selector`: annotation set selector (`Selector.INPUT`, `Selector.REFERENCE`, `Selector.PREDICTION`)
- `equivalences`: equivalence iterator.
- `get_annotation(id_)`: retrieve an annotation by identifier, if not found looks in the input annottation set in the 
same document, if still not found returns `None`.
- `annotations(*args)`: iterate through annotations, `args` can be used to filter the annotations (see examples below).
- `text_bounds(*args)`: iterate through text-bound annotations.
- `normalizations(*args)`: iterate through normalization annotations.
- `relations(*args)`: iterate through relation annotations.

`Annotation` (common to `TextBound`, `Normalization` and `Relation`)
- `aset`: annotation set uplink.
- `id_`: annotation identifier.
- `type_`: annotation type.
- `back_references`: iterator of normalizaions and relations that reference this annotation.

`TextBound`
- `fragments`: tuple of 2-tuple representing fragments (start and end offsets).
- `form`: surface form.
- `multi_span`: fragments as `evaluation.spans.MultiSpan`.
- `__len__()`: length in characters.

`Normalization`
- `annotation`: normalized annotation. Settable only once.
- `referent`: identifier in external resource.

`Relation`
- `arguments`: relation arguments as a `dict`. Result is a copy, modifying it will not affect the annotation.
- `set_argument(role, arg)`: set an argument. Raises a `ValueError` if the argument is already set.

#### Iterating documents in the corpus

```python
for doc in corpus.documents:
    print(doc.docid)
```

#### Iterating annotations

```python
# All input text-bound annotations
for tb in doc.input.text_bounds():
    print(str(tb))

# All reference relations of type "Location"
for loc in doc.reference.text_bounds('Location'):
    print(str(loc))

# All predicted normalizations that satisfy a predicate
for norm in doc.prediction.normalizations(lambda n: n.annotation.type_ == 'Location'):
    print(str(norm))

# Combining type and predicate filters (short locations)
for loc in doc.reference.text_bounds('Location', lambda tb: len(tb) < 20):
    print(str(loc))
```

#### Filters

Wherever filters are allowed, the convertions acts as follows:
- `None` is ignored
- a string is converted into an annotation type filter, for instance `'Location'` accepts annotations of type *Location*.
- a class is converted into an annotation kind filter, for instance `TextBound` accepts text-bound annotations.
- a callable is called to filter the annotations, the callable must accept a single argument (the annotation) and return a boolean.
- an instance of `Selector` accepts annotations in the corresponding annotation set, for instance `Selector.INPUT`
accepts input annotations.
- a list is converted into a sequence of filters composed of the conversion of its elements, the composite filters
accepts annotations that satisfy any one of its elements, for instance `['Location', 'Animal']` accepts annotations of
type *Location* **or** of type *Animal*.

If several filters are provided, then the annotation must satisfy all elements. For instance
`àset.annotations(TextBound, 'Location')` yields all annotations of kind `TextBound` **and** of type *Location*.

The following methods accept filters:
- `AnnotationSet.annotations()`
- `AnnotationSet.text_bounds()`
- `AnnotationSet.nonrmalizations()`
- `AnnotationSet.relations()`
- `Annotation.back_references()`

### Creating a corpus from code

```python
from bionlpst.model import *

corpus = Corpus()
doc = corpus.new_document('README', 'README', 'The fox eats the hen.')  # filename, docid, text content
fox = doc.input.new_text_bound('Animal', ((4, 7),))
hen = doc.input.new_text_bound('Animal', ((17, 20),))
fox_taxon = doc.reference.new_normalization('Taxon', 'Vulpes vulpes', annotation=fox)
hen_taxon = doc.reference.new_normalization('Taxon', 'Gallus gallus domesticus', annotation=hen)
eats = doc.prediction.new_relation('Predation', arguments=(('Predator', fox), ('Prey', hen)))
corpus.write(text_dir='foo/txt', input_dir='foo/in', reference_dir='foo/ref', prediction_dir='foo/pred')
```

### Parsing a reference, then creating and writing a prediction

```python
import bionlpst.parser

parser = bionlpst.parser.BioNLPSTParser()
parser.parse_corpus_and_reference('/path/to/reference')
parser.parse_predictions('/path/to/reference')
corpus = parser.resolve_annotation_references()

doc = list(corpus.documents)[0]  # usually, you would iterate through documents
given = doc.input  # input annotations
pred = doc.prediction  # predicted annotatiosns, empty
# example prediction
entity1 = pred.new_text_bound('Location', [(0, 5)])
entity2 = pred.new_text_bound('Location', [(40, 46)])
norm1 = pred.new_normalization('GeoNames', '2988507', annotation=entity1)
norm2 = pred.new_normalization('GeoNames', '3017382', annotation=entity2)
rel = pred.new_relation('Contains', arguments=[('container', entity2), ('contained', entity1)])

corpus.write(prediction_dir='/path/to/my_redictions')  # will write .a2 files in the specified directory
```